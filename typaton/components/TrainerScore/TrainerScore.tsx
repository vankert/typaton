import "./style.scss"

type Props = {
    score: number,
}

function TrainerScore({score} : Props) {
    return (
        <div className="trainer-score">
          <div className="trainer-score_number">{score}</div>
          <div className="trainer-score_text">symbols</div>
        </div>
    )
}

export default TrainerScore;