import { Metadata } from "next"
import Link from "next/link";
import "./style.scss"

async function getData() {
  const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
    next: {
      revalidate: 60
  }
  });

  return response.json();
}


export const metadata: Metadata = {
    title: 'Articles',
  }

export default async function Articles() {
  const posts = await getData();
    return (
        <div className="articles-page">
          <div className="container">
            <h3>Our Articles</h3>
            <ul>
              {posts.map((post: any) => (
                <li key={post.id}>
                  <Link href={`/articles/${post.id}`}>{post.title}</Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
    )
}