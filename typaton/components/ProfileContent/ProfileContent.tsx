'use client'

import ProfileCard from "@/components/ProfileCard/ProfileCard"
import ProfileScore from "@/components/ProfileScore/ProfileScore"
import ProfileSetting from "@/components/ProfileSetting/ProfileSetting"
import "./style.scss"
import { useSelector} from 'react-redux';
import type { RootState } from "@/redux/store";

function ProfileContent() {
    const currentTab = useSelector((state: RootState) => state.tab.value);
    return (
        <div className="profile-page_content">
            {(currentTab == 'profile')
            ? <ProfileCard/>
            : (currentTab == 'score')
            ? <ProfileScore/>
            : (currentTab == 'setting')
            ? <ProfileSetting/>
            : ''
          }
            
          </div>
    )
}

export default ProfileContent;