'use client'

import {signIn} from "next-auth/react";
import {useSearchParams} from "next/navigation" 
import "./style.scss"

function GoogleButton() {
    const serachParams = useSearchParams();
    const callbackUrl = serachParams.get('callbackUrl') || "/profile";
    
    return (
        <button className="button" onClick={() => signIn('google', {callbackUrl})}>
            Sign In with Google
        </button>
    )
}

export default GoogleButton;