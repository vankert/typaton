import { configureStore } from '@reduxjs/toolkit'
import tabsReducer from './tabsReducer'
import timeReducer from './timeReducer'
import scoreReducer from './scoreReducer'

export const store = configureStore({
  reducer: {
    tab: tabsReducer,
    time: timeReducer,
    score: scoreReducer,
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch