'use client';


import Link from "next/link";
import "./style.scss"
import { useSelector, useDispatch } from 'react-redux';
import type { RootState } from "@/redux/store";
import { resetScore } from "@/redux/scoreReducer";

function FinishBoard() {
    const currentScore = useSelector((state: RootState) => state.score.symbols);
    const dispatch = useDispatch();
    
    return (
        <div className="container">
            <div className="finish-board">
                <h5 className="finish-board_title">Good job!</h5>
                <p className="finish-board_text">Your score is</p>
                <div className="finish-board_score">{currentScore} symbols</div>
                <Link onClick={()=>dispatch(resetScore())} className="button" href='./start'>Try again!</Link>
            </div>
        </div>
    )
}

export default FinishBoard;