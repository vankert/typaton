
import { Metadata } from "next"
import "./style.scss"
import TrainerBoard from "@/components/TrainerBoard/TrainerBoard"

export const metadata: Metadata = {
    title: 'Trainer',
  }


export default function Trainer() {
    return (
      <>
        <TrainerBoard/>
      </>
    )
}