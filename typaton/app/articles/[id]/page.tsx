async function getData(id: string) {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        next: {
            revalidate: 60
        }
    });

    if (!response.ok) {
        // This will activate the closest `error.js` Error Boundary
        throw new Error('Failed to fetch data')
      }

    return response.json();
}

type Props = {
    params: {
        id: string;
    }
}

export async function generateMetadata({ params: {id}}: Props) {
    const post = await getData(id);
    return {
        title: post.title
    }
}

export default async function Post({ params: {id}}: Props) {
    const post = await getData(id);

    return (
        <div className="article-page">
            <div className="container">
                <h1>{post.title}</h1>
                <p>{post.body}</p>
            </div>
        </div>
    )
}