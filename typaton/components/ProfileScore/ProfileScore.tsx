import ScoreTable from "../ScoreTable/ScoreTable";
import "./style.scss"

function ProfileScore() {
    return (
        <div className="profile-score">
            <h3>My Score</h3>
            <ScoreTable/>
        </div>
    )
}

export default ProfileScore;