import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface ProfileTabState {
  value: string
}

const initialState: ProfileTabState = {
  value: 'profile',
}

export const switchProfileTabs = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    profile: (state) => {
      state.value = 'profile'
    },
    score: (state) => {
      state.value = 'score'
    },
    setting: (state) => {
        state.value = 'setting'
      },

    // incrementByAmount: (state, action: PayloadAction<number>) => {
    //   state.value += action.payload
    // },
  },
})

// Action creators are generated for each case reducer function
export const { profile, score, setting } = switchProfileTabs.actions

export default switchProfileTabs.reducer