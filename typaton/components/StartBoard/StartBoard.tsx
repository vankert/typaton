'use client';


import Link from "next/link";
import "./style.scss"
import { useSelector, useDispatch } from 'react-redux';
import type { RootState } from "@/redux/store";
import { oneMinute, threeMinutes, fiveMinutes } from "@/redux/timeReducer";
import { resetScore } from "@/redux/scoreReducer";

function StartBoard() {
    const currentButton = useSelector((state: RootState) => state.time.seconds);
    const dispatch = useDispatch();
    
    return (
        <div className="container">
            <div className="start-board">
                <h5 className="start-board_title">Let&apos;s start your training!</h5>
                <p className="start-board_text">Pick a time</p>
                <ul>
                    <li><button onClick={() => dispatch(oneMinute())} className={(currentButton == 60) ? "active" : ''} type="button">1 minute</button></li>
                    <li><button onClick={() => dispatch(threeMinutes())} className={(currentButton == 180) ? "active" : ''} type="button">3 minutes</button></li>
                    <li><button onClick={() => dispatch(fiveMinutes())} className={(currentButton == 300) ? "active" : ''} type="button">5 minutes</button></li>
                </ul>
                <p className="start-board_text">and</p>
                <Link onClick={()=>dispatch(resetScore())} className="button" href='./trainer'>Let&apos;s go!</Link>
            </div>
        </div>
    )
}

export default StartBoard;