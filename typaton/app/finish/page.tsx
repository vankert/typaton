
import { Metadata } from "next"
import FinishBoard from "@/components/FinishBoard/FinishBoard"

export const metadata: Metadata = {
    title: 'Finish Trainer',
  }


export default function Finish() {
    return (
      <>
        <FinishBoard/>
      </>
    )
}