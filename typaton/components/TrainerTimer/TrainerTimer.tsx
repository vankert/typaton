'use client';

import { useState, useEffect } from 'react';
import { useSelector} from 'react-redux';
import type { RootState } from "@/redux/store";
import { useRouter } from 'next/navigation'

import "./style.scss"

type Props = {
    isStart: boolean
}


function TrainerTimer({isStart} : Props) {
    const currentTime = useSelector((state: RootState) => state.time.seconds);
    const [secondsCount, setSecondsCount] = useState(currentTime);
    const [isFinished, setIsFinished] = useState(false);

    const router = useRouter()

    const finishRoute = () => {
        router.push('/finish');
      }
    

    useEffect(() => {
        let interval: NodeJS.Timeout;
        if (isStart) {
            interval = setInterval(() => {
                setSecondsCount(secondsCount => {
                    if (secondsCount === 0) {
                        clearInterval(interval);
                        setIsFinished(true);
                        return 0;
                    }
                    return secondsCount - 1;
                });
            }, 1000);
        }
        return () => clearInterval(interval);
    }, [isStart]);
    
    useEffect(() => {
        if (isFinished) {
            finishRoute();
        }
    }, [isFinished]);
    
    
    return (
        <div className="trainer-timer">
          <div className="trainer-timer_time">{secondsCount}</div>
          <div className="trainer-timer_text">seconds</div>
        </div>
    )
}

export default TrainerTimer;