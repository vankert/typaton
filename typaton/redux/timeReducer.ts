import { createSlice } from '@reduxjs/toolkit'

export interface trainerTimeState {
    seconds: number
  }

const initialState: trainerTimeState = {
  seconds: 60
}

export const timeReducer = createSlice({
  name: 'time',
  initialState,
  reducers: {
    oneMinute: (state) => {
      state.seconds = 60
    },
    threeMinutes: (state) => {
      state.seconds = 180
    },
    fiveMinutes: (state) => {
      state.seconds = 300
    },
  },
})

// Action creators are generated for each case reducer function
export const { oneMinute, threeMinutes, fiveMinutes } = timeReducer.actions

export default timeReducer.reducer
