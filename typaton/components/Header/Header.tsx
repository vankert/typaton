import Logo from "../Logo/Logo";
import Nav from "../Nav/Nav";
import Profile from "../Profile/Profile";
import "./style.scss"

function Header() {
    return (
        <header className="header">
            <div className="container">
                <Logo/>
                <Nav/>
                <Profile/>
            </div>
        </header>
    )
}

export default Header;
