import Link from "next/link";
import "./style.scss"

function Logo() {
    return (
        <Link className="logo" href='/'>
            Typaton
        </Link>
    )
}

export default Logo;