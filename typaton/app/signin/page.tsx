import GoogleButton from "@/components/GoogleButton/GoogleButton";
import SignInForm from "@/components/SignInForm/SignInForm";
import "./style.scss"

export default async function SignIn() {
    return (
      <div className="signin-page">
        <div className="container">
          <h1>SignIn</h1>
          <GoogleButton/>
          <div>or</div>
          <SignInForm/>
       </div>
      </div>
    )
}