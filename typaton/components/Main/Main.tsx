'use client';
import Header from '@/components/Header/Header'
import Footer from '@/components/Footer/Footer'
import { store } from '@/redux/store'
import { Provider } from 'react-redux'

function Main({
    children,
  }: {
    children: React.ReactNode
  }) {
    return (
        <Provider store={store}>
            <Header/> 
            <main>
              {children}
            </main>
            <Footer/>
        </Provider>
    )
}

export default Main;