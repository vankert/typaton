import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface trainerScoreState {
    symbols: number
  }

const initialState: trainerScoreState = {
  symbols: 0
}

export const scoreReducer = createSlice({
  name: 'score',
  initialState,
  reducers: {
   addScore: (state, action: PayloadAction<number>) => {
      state.symbols += action.payload
    },
    resetScore: (state) => {
        state.symbols = 0
      },
  },
})

// Action creators are generated for each case reducer function
export const { addScore, resetScore } = scoreReducer.actions

export default scoreReducer.reducer
