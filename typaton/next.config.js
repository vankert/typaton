/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ['example.com', 'lh3.googleusercontent.com'],
      },

}

const path = require('path')
 
module.exports = {
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
}

module.exports = nextConfig
