import "./style.scss"
import Image from 'next/image'

export default function Home() {
    return (
        <div className="main-page">
            <div className="container">
                <Image src="/Typa.png" alt="Avatar" width={196} height={241}/>
                <h1>Typaton</h1>
                <h3>Type like a butterfly, learn like a bee!</h3>
                <p>
                    Learn to type quickly with the Typaton keyboard simulator. Use our blind typing
                    lessons.
                </p>
            </div>
        </div>
    )
}
