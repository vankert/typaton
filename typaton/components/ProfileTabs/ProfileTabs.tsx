'use client';

import "./style.scss";
import { useSelector, useDispatch } from 'react-redux';
import type { RootState } from "@/redux/store";
import { profile, score, setting } from "@/redux/tabsReducer";

function ProfileTabs() {
    const currentTab = useSelector((state: RootState) => state.tab.value);
    const dispatch = useDispatch();
    
    return (
        <div className="profile-tabs_wrapper">
            <ul>
                <li>
                    <button onClick={() => dispatch(profile())} className={(currentTab == 'profile') ? "active" : ""}>Profile</button>
                </li>
                <li>
                    <button onClick={() => dispatch(score())} className={(currentTab == 'score') ? "active" : ""}>My score</button>
                </li>
                <li>
                    <button onClick={() => dispatch(setting())} className={(currentTab == 'setting') ? "active" : ""}>Settings</button>
                </li>
            </ul>
        </div>
    )
}

export default ProfileTabs;