import { Metadata } from "next"
import "./style.scss"
import ProfileTabs from "@/components/ProfileTabs/ProfileTabs"
import ProfileContent from "@/components/ProfileContent/ProfileContent"
export const metadata: Metadata = {
    title: 'Privat Area Profile',
  }

export default function Profile() {
  
    return (
      <div className="profile-page">
        <div className="container">
          <ProfileTabs/>
          <ProfileContent/>
        </div>
      </div>
    )
}