import "./style.scss"

function ScoreTable() {
    return (
        <div className="score-table">
            <div className="score-table_header">
                <div className="score-table_row">
                    <div className="score-table_item">Time</div>    
                    <div className="score-table_item">Symbols</div>
                    <div className="score-table_item">Date</div>
                </div>
            </div>
            <div className="score-table_body">
                <div className="score-table_row">
                    <div className="score-table_item">1 minutes</div>
                    <div className="score-table_item">58 symbols</div>
                    <div className="score-table_item">18.06.2023</div>
                </div>
                <div className="score-table_row">
                    <div className="score-table_item">3 minutes</div>
                    <div className="score-table_item">175 symbols</div>
                    <div className="score-table_item">19.06.2023</div>
                </div>
                <div className="score-table_row">
                    <div className="score-table_item">5 minutes</div>
                    <div className="score-table_item">358 symbols</div>
                    <div className="score-table_item">22.07.2023</div>
                </div>
            </div>
        </div>
    )
}

export default ScoreTable;