'use client';

export default function ErrorWrapper({error}: {error: Error}){
    return (
        <div className="container">
            <h1>Oooops!!! {error.message}</h1>
        </div>
    ) 
}