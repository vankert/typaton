'use client';

import Link from "next/link";
import Image from 'next/image'
import "./style.scss"
import { useSession } from "next-auth/react";

function Profile() {
    const session = useSession();

    return (
        <div className="profile">
            {session?.data ?<Link href="/profile">
                <span className="profile_name">{session?.data?.user?.name ?? 'Неизвестно'}</span>
                {session?.data?.user?.image 
                ? <Image src={session.data.user.image} alt="avatar" width={200} height={200} /> 
                : <Image
                    src="/avatar.svg"
                    alt="Avatar"
                    width={30}
                    height={30}
                    />}
                
            </Link> : <Link className="profile_link" href="/signin">
                Login
            </Link>}
            
            
        </div>
    )
}

export default Profile;