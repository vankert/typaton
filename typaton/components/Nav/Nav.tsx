'use client';

import Link from "next/link";
import "./style.scss";
import {usePathname} from 'next/navigation';


function Nav() {
    const pathname = usePathname();
    
    return (
        <nav className="nav">
            <ul>
                <li>
                    <Link className={(pathname == '/start') ? "active" : ""} href='/start'>Trainer</Link>
                </li>
                <li>
                    <Link className={(pathname == '/articles') ? "active" : ""} href='/articles'>Articles</Link>
                </li>
                <li>
                    <Link className={(pathname == '/contacts') ? "active" : ""} href='/contacts'>Contacts</Link>
                </li>
            </ul>
        </nav>
    )
}

export default Nav;