'use client';

import {useSession, signOut} from "next-auth/react"
import Link from "next/link";
import Image from 'next/image'
import "./style.scss"
import ScoreTable from "../ScoreTable/ScoreTable";

function ProfileCard() {
    const session = useSession();
    return (
            <div className="profile-card">
                <div className="profile-card_wrapper">
                    <div className="profile-card_data">
                        {session?.data?.user?.image 
                        ? <Image src={session.data.user.image} alt="avatar" width={100} height={100} />
                        : <Image src="/avatar.svg" alt="avatar" width={100} height={100} />
                    }
                        <span className="profile-card_data-name">{session?.data?.user?.name ?? 'Unknown'}</span>
                    </div>
                    <Link className="button" href="#" onClick={() => {signOut({callbackUrl: "/"})}}>Sign Out</Link>
                </div>

                <div className="profile-card_wrapper">
                    <div className="profile-card_last-score">
                        <h3>My last score</h3>
                        <ScoreTable/>
                    </div>
                </div>
                
            </div>
        
    )
}


export default ProfileCard;