import "./style.scss"

function Footer() {
    return (
        <footer className="footer">
            <div className="container">
                <div className="footer_copyright">copyright: ©Made with love</div>
            </div>
        </footer>
    )
}

export default Footer;
