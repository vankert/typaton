
import { Metadata } from "next"
import "./style.scss"
import StartBoard from "@/components/StartBoard/StartBoard"

export const metadata: Metadata = {
    title: 'Start Trainer',
  }


export default function Start() {
    return (
      <>
        <StartBoard/>
      </>
    )
}