'use client';

import "./style.scss"
import { useRef, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import type { RootState } from "@/redux/store";
import { addScore } from "@/redux/scoreReducer";

import TrainerTimer from "../TrainerTimer/TrainerTimer";
import TrainerScore from "../TrainerScore/TrainerScore";

function TrainerBoard() {
    const [currentText, setCurrentText] = useState('now hush little baby, don\'t you cry everything\'s gonna be alright stiffen that upper lip up little lady, i told ya daddy\'s here to hold ya through the night i know mommy\'s not here right now and we don\'t know why we fear how we feel inside it may seem a little crazy, pretty baby but i promise momma\'s gonna be alright it\'s funny i remember back one year when daddy had no money Mommy wrapped the christmas presents up and stuck \'em under the tree and said some of \'em were from me \'cause daddy couldn\'t buy \'em i\'ll never forget that christmas i sat up the whole night crying \'cause daddy felt like a bum, see daddy had a job');
    const [typedText, setTypedText] = useState('');
    const [isStart, setIsStart] = useState(false);

    const currentScore = useSelector((state: RootState) => state.score.symbols);
    const dispatch = useDispatch();

    const inputRef  = useRef<HTMLInputElement>(null);
    inputRef.current?.focus();

    const handleClick = () => {
        inputRef.current?.focus();
    };

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (!isStart) {
            setIsStart(true);
        }
        if(currentText.length != 0) {
            const currentKey = currentText[0];
            const currentRef = keysRefList[currentKey];
          if(event.key == currentText[0]){
            if (currentRef.current) {
                currentRef.current.classList.remove('trainer_keyboard-key__error');
              }
            setTypedText(typedText + currentText[0])
            setCurrentText(currentText.substring(1))
            dispatch(addScore(1));
          } else {
            if (currentRef.current) {
                currentRef.current.classList.add('trainer_keyboard-key__error');
              }
          }
        } else {
          console.log('Game over');
        }
    }

    const keysRefList: { [key: string]: React.RefObject<HTMLDivElement> } = {
        '`': useRef<HTMLDivElement>(null),
        '1': useRef<HTMLDivElement>(null),
        '2': useRef<HTMLDivElement>(null),
        '3': useRef<HTMLDivElement>(null),
        '4': useRef<HTMLDivElement>(null),
        '5': useRef<HTMLDivElement>(null),
        '6': useRef<HTMLDivElement>(null),
        '7': useRef<HTMLDivElement>(null),
        '8': useRef<HTMLDivElement>(null),
        '9': useRef<HTMLDivElement>(null),
        '0': useRef<HTMLDivElement>(null),
        '-': useRef<HTMLDivElement>(null),
        '=': useRef<HTMLDivElement>(null),

        'q': useRef<HTMLDivElement>(null),
        'w': useRef<HTMLDivElement>(null),
        'e': useRef<HTMLDivElement>(null),
        'r': useRef<HTMLDivElement>(null),
        't': useRef<HTMLDivElement>(null),
        'y': useRef<HTMLDivElement>(null),
        'u': useRef<HTMLDivElement>(null),
        'i': useRef<HTMLDivElement>(null),
        'o': useRef<HTMLDivElement>(null),
        'p': useRef<HTMLDivElement>(null),

        'a': useRef<HTMLDivElement>(null),
        's': useRef<HTMLDivElement>(null),
        'd': useRef<HTMLDivElement>(null),
        'f': useRef<HTMLDivElement>(null),
        'g': useRef<HTMLDivElement>(null),
        'h': useRef<HTMLDivElement>(null),
        'j': useRef<HTMLDivElement>(null),
        'k': useRef<HTMLDivElement>(null),
        'l': useRef<HTMLDivElement>(null),
        ';': useRef<HTMLDivElement>(null),
        '\'': useRef<HTMLDivElement>(null),

        'z': useRef<HTMLDivElement>(null),
        'x': useRef<HTMLDivElement>(null),
        'c': useRef<HTMLDivElement>(null),
        'v': useRef<HTMLDivElement>(null),
        'b': useRef<HTMLDivElement>(null),
        'n': useRef<HTMLDivElement>(null),
        'm': useRef<HTMLDivElement>(null),
        ',': useRef<HTMLDivElement>(null),
        '.': useRef<HTMLDivElement>(null),
        '/': useRef<HTMLDivElement>(null),
        ' ': useRef<HTMLDivElement>(null)
    }

    return (
    <div className="container">
        <TrainerTimer isStart={isStart}/>
        <TrainerScore score={currentScore}/>
        <div className="trainer" onClick={handleClick}>
          <div className="trainer_text">
            <input ref={inputRef} onKeyDown={handleKeyPress} autoFocus type="text" className="trainer_input" />
            <div className="trainer_text-output">{typedText}</div>
            <div className="trainer_text-cursor"></div>
            <div className="trainer_text-input">{currentText}</div>
          </div>
          <div className="trainer_keyboard">
            <div className="trainer_keyboard-row">
              <div ref={keysRefList['`']} className={`trainer_keyboard-key ${(currentText[0] == '`') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">`</div>
              </div>
              <div ref={keysRefList['1']} className={`trainer_keyboard-key ${(currentText[0] == '1') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">1</div>
              </div>
              <div ref={keysRefList['2']} className={`trainer_keyboard-key ${(currentText[0] == '2') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">2</div>
              </div>
              <div ref={keysRefList['3']} className={`trainer_keyboard-key ${(currentText[0] == '3') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">3</div>
              </div>
              <div ref={keysRefList['4']} className={`trainer_keyboard-key ${(currentText[0] == '4') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">4</div>
              </div>
              <div ref={keysRefList['5']} className={`trainer_keyboard-key ${(currentText[0] == '5') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">5</div>
              </div>
              <div ref={keysRefList['6']} className={`trainer_keyboard-key ${(currentText[0] == '6') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">6</div>
              </div>
              <div ref={keysRefList['7']} className={`trainer_keyboard-key ${(currentText[0] == '7') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">7</div>
              </div>
              <div ref={keysRefList['8']} className={`trainer_keyboard-key ${(currentText[0] == '8') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">8</div>
              </div>
              <div ref={keysRefList['9']} className={`trainer_keyboard-key ${(currentText[0] == '9') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">9</div>
              </div>
              <div ref={keysRefList['0']} className={`trainer_keyboard-key ${(currentText[0] == '0') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">0</div>
              </div>
              <div ref={keysRefList['-']} className={`trainer_keyboard-key ${(currentText[0] == '-') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">-</div>
              </div>
              <div ref={keysRefList['=']} className={`trainer_keyboard-key ${(currentText[0] == '=') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">=</div>
              </div>
              <div className="trainer_keyboard-key trainer_keyboard-key__backspace">
                <div className="trainer_key-block">←</div>
              </div>
            </div>
            <div className="trainer_keyboard-row">
              <div className="trainer_keyboard-key trainer_keyboard-key__tab">
                <div className="trainer_key-block">Tab</div>
              </div>
              <div ref={keysRefList['q']} className={`trainer_keyboard-key ${(currentText[0] == 'q') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">q</div>
              </div>
              <div ref={keysRefList['w']} className={`trainer_keyboard-key ${(currentText[0] == 'w') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">w</div>
              </div>
              <div ref={keysRefList['e']} className={`trainer_keyboard-key ${(currentText[0] == 'e') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">e</div>
              </div>
              <div ref={keysRefList['r']} className={`trainer_keyboard-key ${(currentText[0] == 'r') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">r</div>
              </div>
              <div ref={keysRefList['t']} className={`trainer_keyboard-key ${(currentText[0] == 't') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">t</div>
              </div>
              <div ref={keysRefList['y']} className={`trainer_keyboard-key ${(currentText[0] == 'y') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">y</div>
              </div>
              <div ref={keysRefList['u']} className={`trainer_keyboard-key ${(currentText[0] == 'u') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">u</div>
              </div>
              <div ref={keysRefList['i']} className={`trainer_keyboard-key ${(currentText[0] == 'i') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">i</div>
              </div>
              <div ref={keysRefList['o']} className={`trainer_keyboard-key ${(currentText[0] == 'o') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">o</div>
              </div>
              <div ref={keysRefList['p']} className={`trainer_keyboard-key ${(currentText[0] == 'p') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">p</div>
              </div>
              <div className="trainer_keyboard-key">
                <div className="trainer_key-block">&#91;</div>
              </div>
              <div className="trainer_keyboard-key">
                <div className="trainer_key-block">&#93;</div>
              </div>
              <div className="trainer_keyboard-key">
                <div className="trainer_key-block">\</div>
              </div>
            </div>
            <div className="trainer_keyboard-row">
              <div className="trainer_keyboard-key trainer_keyboard-key__caps">
                <div className="trainer_key-block">Caps</div>
              </div>
              <div ref={keysRefList['a']} className={`trainer_keyboard-key ${(currentText[0] == 'a') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">a</div>
              </div>
              <div ref={keysRefList['s']} className={`trainer_keyboard-key ${(currentText[0] == 's') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">s</div>
              </div>
              <div ref={keysRefList['d']} className={`trainer_keyboard-key ${(currentText[0] == 'd') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">d</div>
              </div>
              <div ref={keysRefList['f']} className={`trainer_keyboard-key ${(currentText[0] == 'f') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">f</div>
              </div>
              <div ref={keysRefList['g']} className={`trainer_keyboard-key ${(currentText[0] == 'g') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">g</div>
              </div>
              <div ref={keysRefList['h']} className={`trainer_keyboard-key ${(currentText[0] == 'h') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">h</div>
              </div>
              <div ref={keysRefList['j']} className={`trainer_keyboard-key ${(currentText[0] == 'j') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">j</div>
              </div>
              <div ref={keysRefList['k']} className={`trainer_keyboard-key ${(currentText[0] == 'k') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">k</div>
              </div>
              <div ref={keysRefList['l']} className={`trainer_keyboard-key ${(currentText[0] == 'l') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">l</div>
              </div>
              <div ref={keysRefList[';']} className={`trainer_keyboard-key ${(currentText[0] == ';') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">;</div>
              </div>
              <div ref={keysRefList['\'']} className={`trainer_keyboard-key ${(currentText[0] == '\'') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">&#39;</div>
              </div>
              <div className="trainer_keyboard-key trainer_keyboard-key__enter">
                <div className="trainer_key-block">Enter</div>
              </div>
            </div>
            <div className="trainer_keyboard-row">
              <div className="trainer_keyboard-key trainer_keyboard-key__shift" id="ShiftLeft">
                <div className="trainer_key-block">Shift</div>
              </div>
              <div ref={keysRefList['z']} className={`trainer_keyboard-key ${(currentText[0] == 'z') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">z</div>
              </div>
              <div ref={keysRefList['x']} className={`trainer_keyboard-key ${(currentText[0] == 'x') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">x</div>
              </div>
              <div ref={keysRefList['c']} className={`trainer_keyboard-key ${(currentText[0] == 'c') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">c</div>
              </div>
              <div ref={keysRefList['v']} className={`trainer_keyboard-key ${(currentText[0] == 'v') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">v</div>
              </div>
              <div ref={keysRefList['b']} className={`trainer_keyboard-key ${(currentText[0] == 'b') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">b</div>
              </div>
              <div ref={keysRefList['n']} className={`trainer_keyboard-key ${(currentText[0] == 'n') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">n</div>
              </div>
              <div ref={keysRefList['m']} className={`trainer_keyboard-key ${(currentText[0] == 'm') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">m</div>
              </div>
              <div ref={keysRefList[',']} className={`trainer_keyboard-key ${(currentText[0] == ',') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">,</div>
              </div>
              <div ref={keysRefList['.']} className={`trainer_keyboard-key ${(currentText[0] == '.') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">.</div>
              </div>
              <div className="trainer_keyboard-key">
                <div className="trainer_key-block">/</div>
              </div>
              <div className="trainer_keyboard-key trainer_keyboard-key__shift" id="ShiftRight">
                <div className="trainer_key-block">Shift</div>
              </div>
            </div>
            <div className="trainer_keyboard-row">
              <div ref={keysRefList[' ']} className={`trainer_keyboard-key trainer_keyboard-key__space ${(currentText[0] == ' ') ? 'trainer_keyboard-key__active' : ''}`}>
                <div className="trainer_key-block">Space</div>
              </div>
            </div>
          </div>
        </div>
    </div>
      )
    
}


export default TrainerBoard;